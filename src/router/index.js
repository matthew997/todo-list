import * as All from '../views';

const routes = [
    {
        path: '/',
        component: All.Home,
        exact: true
    },
    {
        path: '/solo/:id',
        component: All.Request,
        exact: true
    },
    {
        path: '/**',
        component: All.PageNotFound,
        exact: false
    }
];

export default routes;
