import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import { RecoilRoot } from 'recoil';
import Loading from './components/loading';

ReactDOM.render(
    <React.StrictMode>
        <RecoilRoot>
            <Suspense fallback={<Loading />}>
                <App />
            </Suspense>
        </RecoilRoot>
    </React.StrictMode>,
    document.getElementById('root')
);
