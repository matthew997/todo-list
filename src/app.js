import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import routes from '@router';

const App = () => {
    return (
        <BrowserRouter>
            <Switch>
                {routes.map((route, i) => (
                    <RouteWithSubRoutes key={i} {...route} />
                ))}
            </Switch>
        </BrowserRouter>
    );
};

const RouteWithSubRoutes = route => {
    return (
        <Route
            route
            path={route.path}
            render={props => (
                <route.component {...props} routes={route.routes} />
            )}
        />
    );
};

export default App;
