import axios from '@config/axios';

export const getTodoList = async () => {
    // @todo - if api supports ORDER BY(ASC|DESC), change it !!!
    const response = await axios.get(`/todos`);
    const { pages } = response.data.meta.pagination;

    const { data } = await axios.get(`/todos`, {
        params: {
            page: pages
        }
    });

    return data;
};

export const getItem = async id => {
    const { data } = await axios.get(`/todos/${id}`);

    return data;
};

export const changeStatusCompleted = async params => {
    const { id, completed } = params;

    const { data } = await axios.put(`/todos/${id}`, { completed });

    return data;
};

export const deleteItem = async id => {
    const { data } = await axios.delete(`/todos/${id}`);

    return data;
};

export const addItemToList = async params => {
    const { data } = await axios.post('/todos', params);

    return data;
};
