import axios from '@config/axios';
import { selectorFamily, atom, selector, atomFamily } from 'recoil';

export const searchAtom = atom({
    key: 'searchAtom',
    default: ''
});

export const hiddenTasksAtom = atom({
    key: 'hiddenTasksAtom',
    default: true
});

export const textAtom = atom({
    key: 'textAtom',
    default: ''
});

export const tasksAtom = atom({
    key: 'tasksAtom',
    default: []
});

export const taskAtom = atomFamily({
    key: 'taskAtom',
    default: {}
});

export const tasksCounter = selector({
    key: 'tasksCounter',
    get: ({ get }) => {
        const items = get(tasksAtom);
        let finished = 0,
            notFinished = 0;

        items.forEach(item => {
            let value = get(taskAtom(item.id));

            if (value.completed) {
                finished += 1;
            } else {
                notFinished += 1;
            }
        });

        return { finished, notFinished };
    }
});

export const findTask = selectorFamily({
    key: 'findTask',
    get:
        id =>
        async ({ get }) => {
            const tasks = get(tasksAtom);

            const index = tasks.findIndex(item => {
                return item.id === id;
            });

            return index;
        }
});

export const textCountAtom = selector({
    key: 'text-count-atom',
    get: ({ get }) => {
        const text = get(textAtom);

        return text.length;
    }
});

export const getTodo = selectorFamily({
    key: 'get-date-todo',
    get:
        id =>
        async ({ get }) => {
            const { data } = await axios.get(`/todos/${id}`);

            return data;
        }
});
