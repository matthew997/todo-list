import React from 'react';
import theme from '@assets/theme';
import { Flex, ThemeProvider, Spinner } from 'theme-ui';

const Loading = () => {
    return (
        <ThemeProvider theme={theme}>
            <Flex
                sx={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: '100vh'
                }}
            >
                <Spinner />
            </Flex>
        </ThemeProvider>
    );
};

export default Loading;
