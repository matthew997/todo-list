import React from 'react';
import { useState } from 'react';
import { Input, Flex, Button, Spinner } from 'theme-ui';
import { textAtom, textCountAtom, tasksAtom, taskAtom } from '@state/todo';
import { addItemToList } from '@state/api';
import { useRecoilState, useRecoilValue, useRecoilCallback } from 'recoil';

const CustomInput = () => {
    const [loading, setLoading] = useState(false);
    const [text, setText] = useRecoilState(textAtom);
    const countedCharacters = useRecoilValue(textCountAtom);

    const insertTask = useRecoilCallback(({ set }) => {
        return newTask => {
            set(tasksAtom, data => [newTask, ...data]);
            set(taskAtom(newTask.id), newTask);
        };
    });

    const add = async () => {
        setLoading(true);
        const params = {
            user_id: 81,
            title: text,
            completed: false
        };

        if (countedCharacters) {
            const { data: newTask } = await addItemToList(params);

            insertTask(newTask);
            setText('');
        } else {
            alert('message');
        }

        setLoading(false);
    };

    const onEnterAddItem = e => {
        if (e.charCode === 13) {
            add();
        }
    };

    return (
        <Flex
            sx={{
                flexDirection: 'row',
                border: '1px solid grey',
                borderRadius: '30px',
                bg: 'foreground',
                width: '100%',
                padding: '2px',
                pl: '15px',
                ':focus': {
                    border: '2px solid red'
                }
            }}
        >
            <Input
                onChange={({ target }) => setText(target.value)}
                onKeyPress={onEnterAddItem}
                value={text}
                placeholder="Hello"
                sx={{
                    bg: 'foreground',
                    fontFamily: 'heading',
                    border: 0,
                    flexGrow: 2,
                    outline: 'none'
                }}
            />

            <Button
                onClick={add}
                mr={2}
                sx={{
                    bg: 'transparent',
                    color: 'text',
                    fontSize: '25px'
                }}
            >
                {loading ? <Spinner sx={{ size: 20 }} /> : '+'}
            </Button>
        </Flex>
    );
};

export default CustomInput;
