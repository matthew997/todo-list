/** @jsxImportSource theme-ui */
import { Button } from 'theme-ui';
import { hiddenTasksAtom } from '@state/todo';
import { useRecoilState } from 'recoil';

const HideButton = () => {
    const [value, setValue] = useRecoilState(hiddenTasksAtom);

    return (
        <div
            sx={{
                p: 3
            }}
        >
            <Button
                onClick={() => {
                    setValue(!value);
                }}
            >
                {value ? 'Hide' : 'Show'}
            </Button>
        </div>
    );
};

export default HideButton;
