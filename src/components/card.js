/** @jsxImportSource theme-ui */
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { Checkbox, Flex, Spinner, Button } from 'theme-ui';
import { useRecoilState, useRecoilValue } from 'recoil';
import { changeStatusCompleted, deleteItem } from '@state/api';
import { taskAtom, hiddenTasksAtom, tasksAtom } from '@state/todo';
import moment from 'moment';

const Card = props => {
    const { id } = props;
    const { completed } = useRecoilValue(taskAtom(id));
    const isVisible = useRecoilValue(hiddenTasksAtom);

    return (
        <div>
            {isVisible ? (
                <CardContent id={id} isDefault="true" />
            ) : (
                isVisible === completed && (
                    <CardContent id={id} isDefault="true" />
                )
            )}
        </div>
    );
};

export const CardContent = props => {
    const { id, isDefault } = props;
    const [data, setValue] = useRecoilState(taskAtom(id));
    const [tasks, setTasksValue] = useRecoilState(tasksAtom);
    const [loading, setLoading] = useState(false);
    const { title, updated_at, completed } = data;

    const setCompleted = async () => {
        setLoading(true);
        const params = {
            id,
            completed: !completed
        };

        const { data } = await changeStatusCompleted(params);

        setValue(data);
        setLoading(false);
    };

    const remove = async () => {
        setLoading(true);
        await deleteItem(id);

        const filtered = tasks.filter(value => {
            return value.id !== id;
        });

        setTasksValue(filtered);

        setLoading(false);
    };

    return (
        <div
            sx={{
                backgroundColor: 'foreground',
                borderRadius: 10,
                fontSize: 4,
                margin: 3,
                padding: 3
            }}
        >
            <Flex sx={{ flexWrap: 'nowrap' }}>
                <Flex
                    onClick={setCompleted}
                    sx={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        p: 3
                    }}
                >
                    {loading ? (
                        <Spinner sx={{ size: 20 }} />
                    ) : (
                        <Checkbox defaultChecked={completed} />
                    )}
                </Flex>

                <Flex sx={{ justifyContent: 'space-between', width: '100%' }}>
                    <div>
                        <h5
                            sx={{
                                fontFamily: 'heading',
                                fontWeight: 'heading',
                                fontSize: [1, 3, 4],
                                margin: 0,
                                textDecoration: completed
                                    ? 'line-through'
                                    : 'none'
                            }}
                        >
                            <Link
                                to={`/solo/${id}`}
                                sx={{
                                    color: 'text',
                                    textDecoration: 'none',
                                    cursor: 'pointer'
                                }}
                            >
                                {title}
                            </Link>
                        </h5>
                        <div
                            sx={{
                                fontFamily: 'heading',
                                fontWeight: 'heading',
                                fontSize: [1, 2],
                                color: 'muted',
                                marginBottom: 2
                            }}
                        >
                            {moment(updated_at).fromNow()}
                        </div>
                    </div>
                    <div
                        sx={{
                            widows: '15px',
                            height: '10px',
                            pt: 2
                        }}
                    >
                        {isDefault && (
                            <Button
                                onClick={remove}
                                sx={{ cursor: 'pointer', fontSize: [1, 3, 4] }}
                            >
                                Delete
                            </Button>
                        )}
                    </div>
                </Flex>
            </Flex>
        </div>
    );
};

export default Card;
