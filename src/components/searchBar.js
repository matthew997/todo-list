/** @jsxImportSource theme-ui */
import theme from '@assets/theme';
import { useRecoilState } from 'recoil';
import { ThemeProvider, Flex, Input, Button } from 'theme-ui';
import { searchAtom } from '@state/todo';

const SearchBar = () => {
    const [value, setText] = useRecoilState(searchAtom);
    const search = () => {};

    return (
        <ThemeProvider theme={theme}>
            <Flex
                sx={{
                    height: '36px',
                    flexDirection: 'row',
                    border: '1px solid grey',
                    borderRadius: '30px',
                    bg: 'foreground',
                    width: '100%',
                    pl: '15px',
                    ':focus': {
                        border: '2px solid red'
                    }
                }}
            >
                <Input
                    onChange={({ target }) => setText(target.value)}
                    value={value}
                    placeholder="Search"
                    sx={{
                        bg: 'foreground',
                        fontFamily: 'heading',
                        border: 0,
                        flexGrow: 2,
                        outline: 'none',
                        color: 'text'
                    }}
                />
                <Button
                    onClick={search}
                    sx={{
                        margin: 0,
                        padding: '0 12px 0',
                        borderRadius: '20px',
                        color: 'text',
                        fontSize: '25px'
                    }}
                >
                    <img
                        src={require('@images/search.svg').default}
                        alt="search"
                        sx={{
                            pt: 1,
                            cursor: 'pointer'
                        }}
                    />
                </Button>
            </Flex>
        </ThemeProvider>
    );
};

export default SearchBar;
