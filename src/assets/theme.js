const theme = {
    fonts: {
        body: 'system-ui, sans-serif',
        heading: '"Avenir Next", sans-serif',
        monospace: 'Menlo, monospace'
    },
    fontSizes: [12, 14, 16, 20, 24, 32, 48],
    fontWeights: {
        body: 400,
        heading: 700
    },
    lineHeights: {
        body: 1.5,
        heading: 1.125
    },
    initialColorMode: 'light',
    colors: {
        text: '#392a25',
        muted: '#aaaaaa',
        background: '#f8f8f8',
        foreground: '#ffffff',
        primary: '#92b955',
        secondary: '#9f8a6d',
        modes: {
            dark: {
                text: '#ffffff',
                background: '#111111',
                foreground: '#333333',
                primary: '#1da1f2'
            }
        }
    },

    space: [0, 4, 8, 16, 32, 48],
    breakpoints: ['40em', '64em', '80em'],
    styles: {
        h1: {
            fontFamily: 'body'
        },
        h2: {
            fontFamily: 'heading'
        },
        p: {
            fontFamily: 'heading'
        }
    }
};

export default theme;
