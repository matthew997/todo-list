/** @jsxImportSource theme-ui */
import { useEffect } from 'react';
import theme from '@assets/theme';
import { useRecoilValue, useRecoilCallback } from 'recoil';
import { ThemeProvider, Container, Flex, Box, Themed } from 'theme-ui';
import ColorSwitch from '@components/colorSwitch';
import CustomInput from '@components/customInput';
import HideButton from '@components/hideButton';
import SearchBar from '@components/searchBar';
import Card from '@components/card';

import { getTodoList } from '@state/api';
import {
    textCountAtom,
    tasksAtom,
    tasksCounter,
    taskAtom,
    searchAtom
} from '@state/todo';

const Home = () => {
    const todosItems = useRecoilValue(tasksAtom);
    const countedCharacters = useRecoilValue(textCountAtom);
    const counter = useRecoilValue(tasksCounter);
    const searchValue = useRecoilValue(searchAtom);

    const insertTasks = useRecoilCallback(
        ({ set }) =>
            data => {
                set(tasksAtom, data);

                for (const item of data) {
                    set(taskAtom(item.id), item);
                }
            },
        []
    );

    useEffect(() => {
        async function fetchAPI() {
            const { data } = await getTodoList();

            insertTasks(data);
        }

        fetchAPI();
    }, [insertTasks]);

    return (
        <ThemeProvider theme={theme}>
            <Container sx={{ p: 4, width: ['100%', '90%', '75%'] }}>
                <ColorSwitch />
                <Flex sx={{ flexWrap: 'wrap', justifyContent: 'center' }}>
                    <Themed.h2>Enjoy your Day :)</Themed.h2>
                    <CustomInput />
                </Flex>
                <Flex
                    sx={{
                        flexWrap: 'wrap',
                        justifyContent: 'space-between',
                        p: 3
                    }}
                >
                    <Themed.p>finished tasks: {counter.finished}</Themed.p>
                    <Themed.p>
                        not finished tasks: {counter.notFinished}
                    </Themed.p>
                    <Themed.p>characters: {countedCharacters}</Themed.p>
                </Flex>
                <Flex>
                    <HideButton />
                    <Flex
                        sx={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            p: 3
                        }}
                    >
                        <SearchBar />
                    </Flex>
                </Flex>
                {todosItems
                    .filter(name => name.title.includes(searchValue))
                    .map((item, index) => (
                        <Box key={index}>
                            <Card id={item.id} />
                        </Box>
                    ))}
            </Container>
        </ThemeProvider>
    );
};

export default Home;
