/** @jsxImportSource theme-ui */
import theme from '@assets/theme';
import { Flex, ThemeProvider, Themed } from 'theme-ui';

function PageNotFound() {
    return (
        <ThemeProvider theme={theme}>
            <Flex
                sx={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: '100vh'
                }}
            >
                <Themed.h1>Page not Found</Themed.h1>
            </Flex>
        </ThemeProvider>
    );
}

export default PageNotFound;
