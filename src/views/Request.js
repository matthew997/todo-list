/** @jsxImportSource theme-ui */
import { useEffect } from 'react';
import theme from '@assets/theme';
import { ThemeProvider, Container } from 'theme-ui';
import { getItem } from '@state/api';
import { taskAtom } from '@state/todo';
import { CardContent } from '@components/card';
import { useRecoilCallback } from 'recoil';

const Request = ({ match }) => {
    const { id } = match.params;

    const insertTask = useRecoilCallback(({ set }) => newTask => {
        set(taskAtom(id), newTask);
    });

    useEffect(() => {
        async function fetchAPI() {
            const { data } = await getItem(id);

            insertTask(data);
        }

        fetchAPI();
    }, [id, insertTask]);

    return (
        <ThemeProvider theme={theme}>
            <Container p={4}>
                <CardContent id={id} />
            </Container>
        </ThemeProvider>
    );
};

export default Request;
