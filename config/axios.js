import axios from 'axios';

axios.defaults.headers.common[
    'Authorization'
] = `Bearer ${process.env.REACT_APP_API_TOKEN}`;

export default axios.create({
    baseURL: process.env.REACT_APP_API
});
